<?php 
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 class Admin_model extends CI_Model
{ 
    /**
     * Check if user exists with given $email and $password
     * @param string $email <p>E-mail</p>
     * @param string $password <p>Password</p>
     * @return mixed : integer user id or false
     */
    public function check_user_data($email, $password)
    {
        $sql = "SELECT * FROM user WHERE email = ? AND role = 'admin'";
        $result = $this->db->query($sql, array($email));
        $user = $result->row_array();
        if (isset($user) && verify_hashed_password($password, $user['password'])) {
            return $user['id'];
        }
        return false;
    }

    /**
     * Return a single result row data of the admin<br>
     * @return object of the admin.
     */
    public function get_admin_by_id($id)
    {
        $query = $this->db->get_where('user', array('id' => $id));
        return $query->row();
    }

    /**
     * Delete the admin
     * @param integer $id <p>id of the admin</p>
     */
    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('user');
    }

    /**
     * Return the query for insert or update the admin<br>
     */
    public function create_or_update($id, $data)
    {
        $data['role'] = 'admin';

        if (empty($id)) {
            return $this->db->insert('user', $data);
        } else {
            $this->db->where('id', $id);
            return $this->db->update('user', $data);
        }
    }

     /**
     * Return array of admins for current page<br>
     * @return array of objects, 
     * or false.
     */
    public function get_current_page_records($limit, $start) 
    {   
        $this->db->where('role', 'admin');
        $this->db->limit($limit, $start);
       return  $this->db->get("user")->result();
    }

    /**
     * Return count of admins<br>
     * @return integer.
     */
    public function get_total_admins() 
    {   
        $this->db->where('role', 'admin');
        return $this->db->count_all_results("user");
    }
}
