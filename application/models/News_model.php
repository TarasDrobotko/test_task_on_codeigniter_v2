<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class News_model extends CI_Model
{

    public function get_news($slug = FALSE)
    {
        if ($slug === FALSE) {
            $query = $this->db->get('news');

            return $query->result_array();
        }

        $query = $this->db->get_where('news', array('slug' => $slug));
        return $query->row_array();
    }

    public function show_news_admin()
    {
        $this->db->from('news');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_post_by_id($id)
    {
        $this->db->from('news');
        $this->db->where('id', $id);
        $query = $this->db->get();

        return $query->row();
    }

    public function create($data)
    {

        $this->db->insert('news', $data);
        return $this->db->insert_id();
    }

    public function update($data, $where)
    {
        $this->db->update('news', $data, $where);
        return $this->db->affected_rows();
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('news');
    }

    public function count_all_news() 
    {
       return $this->db->count_all('news');
    }

    public function get_news_portion($row_per_page, $row_offset) {
        $this->db->limit($row_per_page, $row_offset);

        return $this->db->get('news')->result_array();
    }
}
