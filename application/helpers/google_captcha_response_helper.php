<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('google_captcha_response')) {

    function google_captcha_response()
    {
        $CI =& get_instance();
        
        $secret = $CI->config->item('google_captcha_secret');
        $response = $CI->input->post('g-recaptcha-response');
        $verify_response = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $response);
        $response_data = json_decode($verify_response);
        return $response_data;
    }
}
