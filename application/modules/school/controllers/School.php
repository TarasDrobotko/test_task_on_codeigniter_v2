<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class School extends MX_Controller {

   public function __construct() {
        parent::__construct();
       
    }

    public function get_school_name() {
        return "School N 1";
    }

    public function get_school_location() {
        return "Ukraine, Lutsk";
    }
}