<?php

class Home extends MX_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->module("template");
    }

    public function call_homepage() {
        $home_page = "home/home_view";
        $this->template->loadTemplate($home_page);
    }
}