<div id="main_part" class="container">
    <div class="row">
        <h2 class="h1-responsive font-weight-bolder text-center my-4">Наші контакти</h2>
        <p class="col-sm-12"><strong>Телефон:</strong> +38 066 244 68 56</p>

        <p class="col-sm-12"><strong>E-mail:</strong> drobotkot@gmail.com</p>
        <div id="map_wrapper_div">
            <div id="map_panel" style="left: -250px;"></div>
            <div id="map"></div>
        </div>

        <div class="col-sm-8"><span id="success-msg"></span></div>
        <?php echo form_open(base_url('contacts/send_data'), array('id' => 'ajax-contact-frm', 'class' => 'ajax-contact-form form-horizontal mt-3 col-sm-8')); ?>
        <div class="form-group row">
            <label for="fname" class="control-label col-sm-3">Ім'я</label>
            <div class="col-sm-9">
                <input type="text" id="fname" name="fname" class="form-control" placeholder="Ім'я">
                <div class="tooltip" name="error-fname"></div>
            </div>
        </div>
        <div class="form-group row">
            <label for="email" class="control-label col-sm-3">E-mail</label>
            <div class="col-sm-9">
                <input type="text" name="email" id="email" class="form-control" placeholder="E-mail">
                <div class="tooltip" name="error-email"></div>
            </div>
        </div>
        <div class="form-group row">
            <label for="text" class="col-sm-3 control-label text-top">Текст</label>
            <div class="col-sm-9">
                <textarea id="text" class="form-control" name="text" cols="47" rows="10" placeholder="Tекст"></textarea>
                <div class="tooltip" name="error-text"></div>
            </div>
        </div>
        <div class="form-group row">
            <label for="captcha" class="col-sm-3 control-label">Капча</label>
            <div class="col-sm-9">
                <div class="g-recaptcha" data-callback="recaptchaCallback" data-sitekey="<?php echo $google_captcha_site_key; ?>"></div>
                <div class="tooltip" name="error-captcha"></div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-3"></div>
            <div class="col-sm-9">
                <button class="btn btn-success mb-4" type="submit" id="send-query" name="send-query">Надіслати</button>
            </div>
        </div>
        <?php echo form_close(); ?>

    </div>
</div>