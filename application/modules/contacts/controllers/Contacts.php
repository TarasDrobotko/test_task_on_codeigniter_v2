<?php

class Contacts extends MY_Controller
{
  public function __construct()
  {
    parent::__construct();

    $this->load->library('email');

    $this->load->model('contacts/contacts_model');

    $this->load->helper('language');
    $this->load->helper('change_language');
    $this->load->helper('google_captcha_response');
  }

  public function call_contacts_page()
  {
    $location = $this->contacts_model->get_list();
    $marker = [];
    $infowindow = [];

    foreach ($location as $value) {
      $marker[] = [
        $value->location_name, $value->latitude, $value->longitude
      ];

      $infowindow[] = [
        "<div class=info_content><h3>" . $value->location_name . "</h3><p>" . $value->location_info . "</p>",
        '<i class=\\"fa fa-arrow-left\\" id=arrow-left></i>',
        "</div>"
      ];
    }

    $data['marker'] = json_encode($marker);
    $data['infowindow'] = json_encode($infowindow);


    $contacts_page = "contacts/contacts_view";
    $data['title'] = "Contacts";

    $data['language'] = change_language();
    $data['google_captcha_site_key'] = $this->config->item('google_captcha_site_key');
    
    $this->display_front($contacts_page, $data);
  }

  // send information
  public function send_data()
  {
    $json = array();
    $name = $this->input->post('fname');
    $email = $this->input->post('email');
    $text = $this->input->post('text');

    // name validation
    if (empty(trim($name))) {
      $json['error']['name'] = 'Поле \'Ім\'я\' обов\'язкове для заповнення.';
    }
    // email validation
    if (empty(trim($email))) {
      $json['error']['email'] = 'Поле \'E-mail\' обов\'язкове для заповнення.';
    }
    // check email validation
    if (!empty(trim($email)) && ($this->validate_email($email) == FALSE)) {
      $json['error']['email'] = 'Поле \'E-mail\' має містити правильну поштову адресу Email.';
    }

    // text validation
    if (empty($text)) {
      $json['error']['text'] = 'Поле \'Текст\' обов\'язкове для заповнення.';
    }

    // Google reCAPTCHA verification
    $response_data = google_captcha_response();
    if (!$response_data->success) {
      $json['error']['captcha_error'] = 'reCAPTCHA недійсна.';
    }

    if (empty($json['error'])) {
      //SMTP & mail configuration
      $config = array(
        'protocol' => PROTOCOL,
        'smtp_host' => SMTP_HOST,
        'smtp_port' => SMTP_PORT,
        'smtp_user' => SMTP_USER,
        'smtp_pass' => SMTP_PASS,
        'mailtype' => MAILTYPE,
        'charset' => CHARSET,
      );

      $message = '';
      $body_msg = '<p style="font-size:14px;font-weight:normal;margin-bottom:10px;margin-top:0;">' . $text . '</p>';
      $delimeter = $name . "<br>";
      $data_mail = array('top_msg' => 'Привіт, Тарас!', 'body_msg' => $body_msg, 'thanks_msg' => 'З повагою,', 'delimeter' => $delimeter);

      $this->email->initialize($config);
      $this->email->from($email, $name);
      // set to email
      $this->email->to(TO_MAIL);
      $this->email->subject('Повідомлення з контактної форми');
      // call mail Template
      $message = $this->load->view('mail_template/contact_form', $data_mail, TRUE);
      $this->email->message($message);
      $this->email->send();

      // confirmation mail
      $body_msg = '<p style="font-size:14px;font-weight:normal;margin-bottom:10px;margin-top:0;">Дякую, що зв\'язалися зі мною.</p>';
      $data_mail = array('top_msg' => 'Привіт! ' . $name, 'body_msg' => $body_msg, 'thanks_msg' => 'З повагою,', 'delimeter' => 'Дроботько Тарас.');

      $this->email->initialize($config);
      $this->email->from(TO_MAIL, FROM_TEXT);
      $this->email->to($email);
      $this->email->subject('Підвердження отримання повідомлення з контактної форми');
      //  call confirmation Template
      $message = $this->load->view('mail_template/contact_form', $data_mail, TRUE);
      $this->email->message($message);
      $status = $this->email->send();
      if (!empty($status)) {
        $json['msg'] = 'success';
      } else {
        $json['msg'] = 'failed';
      }
    }
    $this->output->set_header('Content-Type: application/json');
    echo json_encode($json);
  }

  // check email validation
  public function validate_email($email)
  {
    return preg_match('/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', $email) ? TRUE : FALSE;
  }
}
