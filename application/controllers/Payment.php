<?php

class Payment extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('change_language');
        $this->load->helper('language');

        $this->load->model('payment_model', 'payment');

        $this->public_key = $this->config->item('public_key');
        $this->private_key = $this->config->item('private_key');
        $this->patch = './application/logs/callback_robot.php';
    }

    public function donate_form()
    {
        $is_ajax = $this->input->is_ajax_request();
        if ($is_ajax) {
            // clean file
            file_put_contents($this->patch, '');

            $json = array();
            $sum = $this->input->post('donate_sum');
            $email = $this->input->post('donate_email');
            //validation
            $this->form_validation->set_rules(
                'donate_email',
                'Email',
                'required|valid_email',
                array(
                    'required' => 'Поле обов\'язкове для заповнення.',
                    'valid_email' => 'Поле має містити правильну поштову адресу Email.'
                )
            );
            $this->form_validation->set_rules(
                'donate_sum',
                'Donate sum',
                'required|integer|is_natural_no_zero',
                array(
                    'required' => 'Поле обов\'язкове для заповнення.',
                    'integer' => 'Поле має містити ціле число.',
                    'is_natural_no_zero' => 'Введена сума має бути більшим нуля цілим числом.'
                )
            );
            if ($this->form_validation->run() == FALSE) {
                $errors = array();
                // Loop through $_POST and get the keys
                foreach ($this->input->post() as $key => $value) {
                    // Add the error message for this field
                    $errors[$key] = form_error($key);
                }
                $json['error'] = array_filter($errors); // Some might be empty
            }

            // variant 1 of the validation
            // sum validation
            // if (empty(trim($sum))) {
            //     $json['error']['sum'] = 'Поле обов\'язкове для заповнення.';
            // }

            // if (empty(trim($sum)) && (trim($sum) == '0')) {
            //     $json['error']['sum'] = 'Сума пожертвування має бути більшою, ніж "0".';
            // }

            // if (
            //     !empty(trim($sum))
            //     && ((preg_match("#^0+$#", $sum) or $this->isInteger(trim($sum)) == FALSE))
            // ) {
            //     $json['error']['sum'] = 'Поле має містити ціле число.';
            // }

            // if (is_numeric($sum) && trim($sum) < 0) {
            //     $json['error']['sum'] = "Число має бути додатним.";
            // }

            // // email validation
            // if (empty(trim($email))) {
            //     $json['error']['email'] = 'Поле обов\'язкове для заповнення.';
            // }
            // // check email validation
            // if (!empty(trim($email)) && ($this->validate_email($email) == FALSE)) {
            //     $json['error']['email'] = 'Поле має містити правильну поштову адресу Email.';
            // }

            if (empty($json['error'])) {

                $liqpay = new LiqPay($this->public_key, $this->private_key);
                $order_id = 'donate_' . rand(10000, 99999);

                $json['form'] = $liqpay->cnb_form(array(
                    'action' => 'pay',
                    'version'        => '3',
                    'amount'         => $sum,
                    'currency'       => 'UAH',
                    'description'    => 'Donate Drobotko T. V.',
                    'order_id'       => $order_id,
                    'language'      => 'uk',
                    'result_url'    => base_url() . 'payment/success_donate',
                    'server_url'    => base_url() . 'payment/success_donate',
                    'sandbox'       => 1
                ));

                $data = array(
                    'order_id'  => $order_id,
                    'email'   => $email,
                    'sum' => $sum,
                );
                //add donate to database
                $this->payment->add_donate($data);

                echo json_encode($json);
            } else {
                $json['status'] = 'error';
                echo json_encode($json);
            }
        }
    }

    public function success_donate()
    {
        $data['message'] = $this->get_liqpay_status();
       
        $data['title'] = "Cтатус пожертвування";
        $data['language'] = change_language();

        $this->display_front('payment/success_donate', $data);
    }
    // variant 1 of the validation
    // check email validation
    // public function validate_email($email)
    // {
    //     return preg_match('/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', $email) ? TRUE : FALSE;
    // }

    // public function isInteger($sum)
    // {
    //     return (ctype_digit(strval($sum)));
    // }

    public function get_liqpay_status()
    {
            file_put_contents($this->patch, var_export($this->input->post(NULL, FALSE), true) . "\r\n\r\n", FILE_APPEND | LOCK_EX);
            $data_get = file_get_contents($this->patch);
            $data_arr = explode('=> ', $data_get);
            if(empty($data_arr[1])) return 'Вибачте, дані про переказ не були передані!';
            $data_sign = explode("'", $data_arr[1]);
            $data_sign = $data_sign[1];

            $data_data = explode("'", $data_arr[2]);
            $data_data = trim($data_data[1]);

            $sign = base64_encode(sha1(
                $this->private_key .
                    $data_data .
                    $this->private_key,
                1
            ));

            if ($sign == $data_sign) {
                // get liqpay order_id
                $order_id = $this->get_liqpay_order_id($data_data);
                //add donate success status to database  
                $data = array(
                    'status' => 1,
                    'order_id' => $order_id,
                );
                $this->payment->add_donate_status($data);

                return 'Дякуємо, пожертвування зроблене успішно!';
            } else {
                return  'Вибачте, помилка під час переказу коштів!';
            }
    }

    public function get_liqpay_order_id($data_data)
    {
        $liqpay_data = base64_decode($data_data);
        $jsonObj = json_decode($liqpay_data);
        $order_id = $jsonObj->order_id;
        return $order_id;
    }
}
