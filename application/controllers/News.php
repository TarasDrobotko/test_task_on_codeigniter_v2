<?php

class News extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('my_img_manipulations');
        $this->load->library('pagination');
        $this->load->model('news_model');

        $this->load->helper('language');
        $this->load->helper('insert_advertising_helper');
        $this->load->helper('change_language');
    }

    public function index()
    {
        $data['title'] = "News";
        $data['news'] = $this->news_model->get_news();

        $data['language'] = change_language();

        $data['donate_block'] = $this->load->view('sections/donate_block', NULL, TRUE);
        
        $this->display_front('news/index', $data);
    }

    public function view($slug = NULL)
    {
        $data['language'] = change_language();

        $data['news_item'] = $this->news_model->get_news($slug);

        if (empty($data['news_item'])) {
            show_404();
        }
        $data['title'] = $data['news_item']['title'];
        $data['image_file'] = $data['news_item']['image'];
        $data['image_alt'] = $data['news_item']['image_alt'];
        $data['short_content'] = $data['news_item']['short_content'];
        $data['text'] = $data['news_item']['text'];

        $text_news =  $data['short_content'] . $data['text'];
        $data['text_advertising'] = insert_reclama_in_html($text_news);

        $this->display_front('news/view', $data);
    }

    public function list_news()
    {
        $data['title'] = "News list";
        
        if (!$this->session->userdata("user")) return redirect('/admin/admin_login');

        $this->display_admin('news/list', $data);
    }

    public function news_admin_data()
    {
        $data = $this->news_model->show_news_admin();
        echo json_encode($data);
    }

    public function get_post_by_id()
    {
        $id = $this->input->post('id');

        $data = $this->news_model->get_post_by_id($id);

        $arr = array('success' => false, 'data' => '');
        if ($data) {
            $arr = array('success' => true, 'data' => $data);
        }
        echo json_encode($arr);
    }

    public function store()
    {   
        //upload image
        $this->load->config('upload', TRUE);
        $upload_vals = $this->config->item('upload_1', 'upload');
        $this->load->library('upload', $upload_vals);

        if ($this->input->post('hid_img_name') != '') {
            $image = $this->input->post('hid_img_name');
        } else if ($this->upload->do_upload('image_file')) {
            $data_image = array('upload_data' => $this->upload->data());

            $image = $data_image['upload_data']['file_name'];

            $this->my_img_manipulations->resize_and_compress_img($image);
            $upload_patch = $upload_vals['upload_path'];
            // change permission to image
            chmod($upload_patch . $image, 0777);
        }


        $data = array(
            'title' => $this->input->post('title'),
            'image' => $image,
            'image_alt' => $this->input->post('image_alt'),
            'short_content' => $this->input->post('short_content'),
            'text' => $this->input->post('text'),
            'slug' => $this->input->post('slug'),
            'created_at' => date('Y-m-d H:i:s'),
        );

        $status = false;

        $id = $this->input->post('post_id');

        if ($id) {
            $post = $this->news_model->get_post_by_id($id);
            $img_file = $post->image;
            unlink('./uploads/' . $img_file);

            $where = array('id' => $this->input->post('post_id'));
            $this->news_model->update($data, $where);

            $status = true;
        } else {
            $id = $this->news_model->create($data);
            $status = true;
        }

        $data = $this->news_model->get_post_by_id($id);

        echo json_encode(array("status" => $status, 'data' => $data));
    }

    public function delete()
    {
        $id = $this->input->post('post_id');
        $post = $this->news_model->get_post_by_id($id);
        $image_file = $post->image;
        unlink('./uploads/' . $image_file);

        $this->news_model->delete($id);
        echo json_encode(array("status" => TRUE));
    }

    public function upload_cropped_image()
    {
        $this->my_img_manipulations->upload_cropped_image();
    }

    /**
     * get data for load news with pagination for the main page
     * @return JSON encoded string or FALSE
     */

    public function get_data_records($row_offset = 0)
    {
        $row_per_page = 5;

        if ($row_offset != 0) {
            $row_offset = ($row_offset - 1) * $row_per_page;
        }

        $all_count_news = $this->news_model->count_all_news();
        $news_portion = $this->news_model->get_news_portion($row_per_page, $row_offset);

        $config['base_url'] = base_url() . 'news/get_data_records';
        $config['total_rows'] = $all_count_news;
        $config['per_page'] = $row_per_page;

        $this->pagination->initialize($config);

        $data['pagination'] = $this->pagination->create_links();
        $data['result'] = $news_portion;
        $data['row'] = $row_offset;

        echo json_encode($data);
    }
}
