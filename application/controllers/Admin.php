<?php
class Admin extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('admin_model');

        $this->load->library('pagination');

        $this->load->helper('password_helper');
        $this->load->helper('google_captcha_response');
    }

    public function admin_login()
    {   
        $data['google_captcha_site_key'] = $this->config->item('google_captcha_site_key');
        $this->display_admin('admin_login/admin_login', $data);
    }

    public function submit()
    {
        $email = $this->input->post('email');

        $password = $this->input->post('password');

        // validation
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

        // Google reCAPTCHA verification
        $response_data = google_captcha_response();
        $errors = [];
        if (!$response_data->success) {
            $this->session->set_userdata(array('message' => 'reCAPTCHA input is not valid.'));
            $errors[] = ['captcha_error' => 'reCAPTCHA input is not valid.'];
        }

        if ($this->form_validation->run() == FALSE) {
            $errors_validation = validation_errors();
            $errors[] = ['error' => $errors_validation];
        }

        //check if the user exist
        $user_id = $this->admin_model->check_user_data($email, $password);
        if ($user_id === FALSE) {
            $errors[] = ['error_login' => "Incorrect login information or user isn\'t admin."];
        }

        if ($this->form_validation->run() == TRUE && $user_id) {

            $this->auth($user_id);
        }
        echo json_encode($errors);
    }

    public function admin_page()
    {
        // init params of the pagination
        $limit_per_page = 2;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->admin_model->get_total_admins();

        if ($total_records > 0 && is_numeric($start_index)) {
            // get current page records
            $data["admins_list"] = $this->admin_model->get_current_page_records($limit_per_page, $start_index);

            $config['base_url'] = base_url() . 'admin/admin_page';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;

            $this->pagination->initialize($config);

            // build paging links
            $data["links"] = $this->pagination->create_links();
        }
        $data['title'] = 'Admins list';

        $this->check_logged();

        $this->display_admin('admin_page/admin_page', $data);
    }

    /**
     * remove data about admin from session
     */
    public function admin_logout()
    {
        $this->session->unset_userdata('user');

        header("Location: /admin/admin_login");
    }

    /**
     * CRUD: create admin
     */
    public function create()
    {
        $data['title'] = 'Add admin';

        $this->check_logged();

        $this->display_admin('admin_page/create', $data);
    }

    /**
     * CRUD: delete admin
     */
    public function delete()
    {
        $id = $this->uri->segment(3);
        if (empty($id)) {
            show_404();
        }
        if (is_numeric($id)) {
        $this->admin_model->delete($id);
        redirect(base_url('admin/admin_page'));
        }
    }

    /**
     * CRUD: edit admin
     */
    public function edit($id)
    {
        $id = $this->uri->segment(3);

        if (empty($id)) {
            show_404();
        } else if (is_numeric($id)) {
            $data['admin'] = $this->admin_model->get_admin_by_id($id);
            $data['title'] = 'Edit admin';

            $this->check_logged();

            $this->display_admin('admin_page/edit', $data);
        }
    }

    /**
     * store admin
     */
    public function store()
    {
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[20]');

        $id = $this->input->post('id');

        if ($this->form_validation->run() == FALSE) {
            if (empty($id)) {
                $data['title'] = 'Add admin';
                $this->session->set_flashdata('error', validation_errors());
                $this->session->set_flashdata('name', $this->input->post('name'));
                $this->session->set_flashdata('email', $this->input->post('email'));
                $this->session->set_flashdata('password', $this->input->post('password'));
                redirect(base_url('admin/create'));
            } else {
                $this->session->set_flashdata('error', validation_errors());
                $this->session->set_flashdata('password', $this->input->post('password'));
                redirect(base_url('admin/edit/' . $id));
            }
        } else {
            $password = $this->input->post('password');
            $password_hash = get_hashed_password($password);

            $data = array(
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'password' => $password_hash
            );

            $id = $this->input->post('id');

            $this->admin_model->create_or_update($id, $data);
            redirect(base_url('admin/admin_page'));
        }
    }

    /**
     * Remember the user
     * @param integer $user_id <p>id of the user</p>
     */
    public function auth($user_id)
    {
        // Write identifier of the user in session
        $this->session->set_userdata('user', $user_id);
    }

    /**
     * Check if user is logged <br/>
     * redirect to login page if user not logged 
     * @return void
     */
    public function check_logged()
    {
        if (!$this->session->userdata("user")) return redirect('/admin/admin_login');
    }
}
