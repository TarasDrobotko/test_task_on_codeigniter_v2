<div id="main_part" class="container">
    <div class="row">
         <!-- Grid column -->
	 <div class="col-lg-5">

<!-- Featured image -->
<div class="view overlay mb-lg-5 mb-4 mt-4">
  <img class="img-fluid rounded z-depth-3" src="<?php echo base_url("uploads/$image_file"); ?>" 
  alt="<?php echo $image_alt; ?>">

</div>

</div>
<div class="col-lg-7">
<h3 class="font-weight-bold mb-3 mt-4"><?php echo $title; ?></h3>
<?php 
echo html_entity_decode($text_advertising); ?>
</div>
    </div>
</div>