<footer id="blog_footer" class="bg-light">
  <div class="text-center py-3">
  <?php echo lang('copyright'); ?> © <?php echo date('Y'); ?>
  <?php echo lang('drobotko'); ?>
  </div>
</footer>
 
<?php if ($this->uri->segment(1) == "") { ?>
<script src="<?php echo base_url('assets/js/pagination_ajax.js');?>"></script>
<?php } ?>
<?php if ($this->uri->segment(1) == "contacts") { ?>
<script src="<?php echo base_url('assets/js/map.js'); ?>"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBw5fb5QBtHpdaTx6LdMZkNgmBdQvGDwbg&callback=initMap">
</script>
<?php } ?>
<script src="<?php echo base_url('assets/js/liqPay-donate.js');?>"></script>
<script src="<?php echo base_url('assets/js/search_posts.js');?>"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
</body>

</html>