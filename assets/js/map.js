// Initialize and add the map
function initMap() {
	// The location of Uluru
	var uluru = {
		lat: +markerMap[0][1],
		lng: +markerMap[0][2]
	};
	// The map, centered at Uluru
	var map = new google.maps.Map(document.getElementById("map"), {
		zoom: 16,
		scaleControl: true,
		center: uluru,
		mapTypeControl: true,
		mapTypeControlOptions: {
			style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
			position: google.maps.ControlPosition.TOP_CENTER
		}
	});
	// The marker, positioned at Uluru
	var marker = new google.maps.Marker({
		position: uluru,
		map: map,
		title: markerMap[0][0]
	});

	infoWindowContentPanel = infoWindowContent[0].join("");

	// Display multiple markers on a map
	infoWindow = new google.maps.InfoWindow();

	marker.addListener("click", function() {
		var minleft = -250;
		var maxleft = 0;
		var time = 1000;
		var timer = null;

		var el = document.getElementById("map_panel");
		var init = new Date().getTime(); //start time
		clearInterval(timer);
		var instanceleft = parseInt(el.style.left); // Current left position
		var left = instanceleft == -250 ? maxleft : minleft;

		el.innerHTML = infoWindowContentPanel;

		var arrow = document.getElementById("arrow-left");
		arrow.onclick = function() {
			map_panel.style["transition"] = "left 1s ease-in-out";
			map_panel.style.left = "-250px";
		};

		//variant 1 with display
		// el.style.display = (el.style.display == 'none') ? 'block' : 'none';
		// if (el.style.display == 'block') {
		//var content = infoWindowContent[0][0];
		//el.innerHTML = content;
		// }

		var disp = left - parseInt(el.style.left);
		timer = setInterval(function() {
			var instance = new Date().getTime() - init; //animating time
			if (instance <= time) {
				//0 -> time seconds
				var pos = instanceleft + Math.floor((disp * instance) / time);
				el.style.left = pos + "px";
			} else {
				el.style.left = left + "px"; 
				clearInterval(timer);
			}
		}, 1);
	});

	google.maps.event.addListener(
		marker,
		"click",
		(function(marker) {
			return function() {
				var content_tip = infoWindowContent[0][0] + infoWindowContent[0][2];
				infoWindow.setContent(content_tip);
				infoWindow.open(map, marker);
			};
		})(marker)
	);
}
