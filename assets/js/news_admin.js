var SITEURL = document.location.origin + "/";

$(document).ready(function() {
	show_news();

	//get selected image name
	$('input[type="file"]').change(function(e) {
		image_name = e.target.files[0].name;
	});

	//crop image
	$image_crop = $("#image_demo").croppie({
		enableExif: true,
		viewport: {
			width: 200,
			height: 200,
			type: "square" //circle
		},
		boundary: {
			width: 300,
			height: 300
		},
		enableResize: true,
		enableOrientation: true
	});

	$("#image").on("change", function() {
		var reader = new FileReader();
		reader.onload = function(event) {
			$image_crop
				.croppie("bind", {
					url: event.target.result
				})
				.then(function() {
					console.log("jQuery bind complete");
				});
		};
		reader.readAsDataURL(this.files[0]);
		$("#uploadimageModal").modal("show");
	});

	$(".crop_image").click(function(event) {
		$image_crop
			.croppie("result", {
				type: "canvas",
				size: "viewport"
			})
			.then(function(response) {
				$.ajax({
					url: SITEURL + "news/upload_cropped_image",
					type: "POST",
					data: {
						image: response,
						image_name: image_name
					},
					dataType: "json",
					success: function(res) {
						$("#blah").attr("src", SITEURL + "uploads/" + res.data);
						$("#uploadimageModal").modal("hide");
						$("#hid_img_name").val(res.data);
						$("#img_name").html(res.data);
						$("#slug").focus();
					}
				});
			});
	});

	//function show all news
	function show_news() {
		$.ajax({
			type: "ajax",
			url: SITEURL + "/news/news_admin_data",
			async: true,
			dataType: "json",
			success: function(data) {
				// If table is initialized
				if ($.fn.DataTable.isDataTable("#post_list")) {
					// Destroy existing table
					$("#post_list")
						.DataTable()
						.destroy();
				}
				var html = "";
				var i;
				for (i = 0; i < data.length; i++) {
					html +=
						'<tr id="post_id_' +
						data[i].id +
						'">' +
						"<td>" +
						data[i].id +
						"</td>" +
						"<td>" +
						data[i].title +
						"</td>" +
						"<td>" +
						data[i].image +
						"</td>" +
						"<td>" +
						'<a href="javascript:void(0);" id="edit-post" class="btn btn-info" data-id="' +
						data[i].id +
						'">Edit</a>' +
						" " +
						'<a href="javascript:void(0);" id="delete-post" class="btn btn-danger delete-user" data-id="' +
						data[i].id +
						'">Delete</a>' +
						"</td>" +
						"</tr>";
				}
				$("#show_data").html(html);
				// Initialize the table
				$("#post_list").DataTable({
					lengthMenu: [
						[3, 5, 10, 25, 50, -1],
						[3, 5, 10, 25, 50, "All"]
					]
				});
			}
		});
	}

	/*  When user click add user button */

	$("#create-new-post").click(function() {
		$("#btn-save").val("create-post");
		$("#post_id").val("");
		$("#img_name").html("");
		$("#hid_img_name").val("");
		$("#blah").hide();
		$("#postForm").trigger("reset");
		$("#postCrudModal").html("Add New Post");
		$("#ajax-post-modal").modal("show");
	});

	/* When click edit user */

	$("body").on("click", "#edit-post", function() {
		var post_id = $(this).data("id");

		$.ajax({
			type: "Post",
			url: SITEURL + "news/get_post_by_id",
			data: {
				id: post_id
			},
			dataType: "json",
			success: function(res) {
				if (res.success == true) {
					$("#blah").show();
					//reset error
					$("label.error").remove();
					$(".form-control").removeClass("error");

					$("#blah").attr("src", SITEURL + "uploads/" + res.data.image);
					$("#blah").attr("alt", res.data.image_alt);
					$("#img_name").html(res.data.image);
					$("#hid_img_name").val("");
					$("#postCrudModal").html("Edit Post");
					$("#btn-save").val("edit-post");
					$("#image").val("");
					$("#ajax-post-modal").modal("show");
					$("#post_id").val(res.data.id);
					$("#title").val(res.data.title);
					$("#image_alt").val(res.data.image_alt);
					$("#short_content").val(res.data.short_content);
					$("#text").val(res.data.text);
					$("#slug").val(res.data.slug);
				}
			},
			error: function(data) {
				console.log("Error:", data);
			}
		});
		$('input[type="file"]').change(function(e) {
			var image_name = e.target.files[0].name;
			$("#img_name").html(image_name);
		});
	});

	$("body").on("click", "#delete-post", function() {
		var post_id = $(this).data("id");

		if (confirm("Are You sure want to delete?")) {
			$.ajax({
				type: "Post",
				url: SITEURL + "news/delete",
				data: {
					post_id: post_id
				},
				dataType: "json",
				success: function(data) {
					$("#post_id_" + post_id).remove();
					show_news();
				},
				error: function(data) {
					console.log("Error:", data);
				}
			});
		}
	});

	if ($("#postForm").length > 0) {
		$("#postForm").validate({
			submitHandler: function(form) {
				var actionType = $("#btn-save").val();

				$("#btn-save").html("Sending..");

				$.ajax({
					url: SITEURL + "news/store",
					method: "POST",
					data: new FormData(form),
					dataType: "json",
					contentType: false,
					cache: false,
					processData: false,
					success: function(res) {
						var post =
							'<tr id="post_id_' +
							res.data.id +
							'"><td>' +
							res.data.id +
							"</td><td>" +
							res.data.title +
							"</td><td>" +
							res.data.image +
							"</td>";
						post +=
							'<td><a href="javascript:void(0)" id="edit-post"  data-id="' +
							res.data.id +
							'" class="btn btn-info mr-1">Edit</a><a href="javascript:void(0)" id="delete-product" data-id="' +
							res.data.id +
							'" class="btn btn-danger delete-user">Delete</a></td></tr>';

						if (actionType == "create-post") {
							show_news();
						} else {
							$("#post_id_" + res.data.id).replaceWith(post);
						}

						$("#postForm").trigger("reset");
						$("#ajax-post-modal").modal("hide");
						$("#btn-save").html("Save Changes");
					},
					error: function(data) {
						console.log("Error:", data);
						$("#btn-save").html("Save Changes");
					}
				});
			}
		});
	}
});

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {
			$("#blah").show();
			$("#blah").attr("src", e.target.result);
			$("#blah").attr("alt", "your image");
		};

		reader.readAsDataURL(input.files[0]);
	}
}
