var BASE_URL = document.location.origin+'/';

$(document).ready(function() {
  $("#search").autocomplete({
    source: function(request, response) {
      $.ajax({
        url: BASE_URL + "search_news/search",
        data: {
          term: request.term
        },
        dataType: "json",
        success: function(data) {
          dataNews = data;
          var resp = $.map(data, function(obj) {
            return obj.title;
          });

          response(resp);
        },
      });
    },
    select: function(event, ui) {
      let i;
      for (i = 0; i < dataNews.length; i++) {

        if (dataNews[i].title == ui.item.label) {
          var slug = dataNews[i].slug;
        }

      }
      window.location = BASE_URL + 'view/' + slug;
    },
    minLength: 1
  });
});