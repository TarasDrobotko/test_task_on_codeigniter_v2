$(document).ready(function() {

   $('#pagination').on('click', 'a', function(e) {

      e.preventDefault();

      var pageno = $(this).attr('data-ci-pagination-page');

      loadPagination(pageno);

   });

   loadPagination(0);

   function loadPagination(pagno) {
      $.ajax({
         url: '/news/get_data_records/' + pagno,
         type: 'get',
         dataType: 'json',
         success: function(response) {
            $('#pagination').html(response.pagination);
            createPosts(response.result, response.row);
         }
      });

   }

   function createPosts(result, sno) {

      sno = Number(sno);

      $('.post').empty();
      var SITEURL = document.location.origin+'/';

      for (index in result) {

         var id = result[index].id;
         var title = result[index].title;
         var slug = result[index].slug;
         var short_content = result[index].short_content;
         var image = result[index].image;
         var image_alt = result[index].image_alt;

         sno += 1;

         var post = '<div class="row">';
         post += '<div class="col-lg-5">';
         post += '<div class="view overlay mb-lg-0 mb-4 ">';
         post += '<img class="img-fluid rounded z-depth-3" src="' + SITEURL + 'uploads/' + image + '" alt="' + image_alt + '">';
         post += '</div>';
         post += '</div>';
         post += '<div class="col-lg-7">';
         post += '<h3 class="font-weight-bold mb-3"> <a href="view/' + slug + '">';
         post += title + '</a> </h3>';
         post += short_content;
         post += '<a class="btn btn-success btn-md" href="view/' + slug + '">Читати далі </a>';
         post += '</div>';
         post += '</div>';
         post += '<hr class="my-5">';

         $('.post').append(post);
      }
   }
});